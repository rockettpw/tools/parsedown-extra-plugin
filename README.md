# Parsedown Extra Plugin for ProcessWire

TextformatterParsedownExtraPlugin is fast markdown textformatter that uses [Parsedown Extra Plugin](https://github.com/tovic/parsedown-extra-plugin), which extends Parsedown and Parsedown Extra to provide more options for HTML output.

## Documentation

To add classes, IDs, and other attributes to an element that is not a paragraph, simply suffix that element with `{.class #id}` for classes and IDs or `{attr=something}` for attributes. For example, end a heading like this: `## Heading {.clause}`, or a link like this `[Link](http://url/) {.link}`. [Learn more about this syntax](https://github.com/tovic/parsedown-extra-plugin#advance-attribute-parser).

For code blocks, you may now use dot notation (````.php html`) to set the language and add a class, or even specify your own class and ID (````{.code-block #baz}`). [Learn more about this syntax](https://github.com/tovic/parsedown-extra-plugin#code-block-class-without-language--prefix).

See the module config for information.

---

The textformatter is licensed under the [ISC License](LICENSE.md). The Parsedown packages that the textformatter uses, authored by Emanuil Rusev, are licensed under the [MIT License](https://github.com/erusev/parsedown/blob/master/LICENSE.txt).
